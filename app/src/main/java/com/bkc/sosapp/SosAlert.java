package com.bkc.sosapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bkc.womensecurityapp.R;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;

public class SosAlert extends AppCompatActivity{

    SessionManager sessionManager;
    TextView cancelTV;
    Button cancelBtn;
    Handler handler = new Handler();
    String mylatitude,mylongitude;
    TextView TVlatitude,TVlongitude;
    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;

    private static final long REDIRECT_HOME = 8000;
    private static final long HELP_FUNCTION_START = 6000;

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 20000;

    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 15000;
    private static final int REQUEST_CHECK_SETTINGS = 100;
    Boolean executeOnResume = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos_alert);

        sessionManager = new SessionManager(this);
        TVlatitude = findViewById(R.id.latitudeTV);
        TVlongitude = findViewById(R.id.longitudeTV);

        reverseTimer(5,cancelTV);
        init();
        checkPermissions();
        sosBackHome();

        cancelTV = findViewById(R.id.cancelSos);
        cancelBtn = findViewById(R.id.cancelSosBtn);

        ButterKnife.bind(this);

        // Starting Location update and sms send APi under some time delay
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startLocationUpdates();
                sendEmail();
                sendSMS();
            }
        },HELP_FUNCTION_START);

        /* effets on the button*/
        final RippleBackground rippleBackground=(RippleBackground)findViewById(R.id.cancelripple);
        rippleBackground.startRippleAnimation();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeOnResume = false;
                handler.removeCallbacksAndMessages(null);
                Intent intent = new Intent(getApplicationContext(),Home.class);
                startActivity(intent);
                finish();
            }
        });

        restoreValuesFromBundle(savedInstanceState);

    }

    private void sendSMS() {

    }

    public void reverseTimer(int Seconds, final TextView tv) {

        new CountDownTimer(Seconds * 1000 + 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);

                int hours = seconds / (60 * 60);
                int tempMint = (seconds - (hours * 60 * 60));
                int minutes = tempMint / 60;
                seconds = tempMint - (minutes * 60);

                cancelTV.setText(String.format("%d", seconds));
            }

            public void onFinish() {
                cancelTV.setText("00");

            }
        }.start();
    }

    // Redirecting back to Home activity after some delay
    public void sosBackHome(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(),Home.class);
                startActivity(intent);
            }
        },REDIRECT_HOME);
    }


    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location received
                mCurrentLocation = locationResult.getLastLocation();


                updateLocationUI();
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    // Restoring values from saved instance state
    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }
        }
    }


    public void stopLocationUpdate(){
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getApplicationContext(), "Help Alert Cancelled!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
    }

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("Missing Permission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                        if (ActivityCompat.checkSelfPermission(SosAlert.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SosAlert.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                            return;
                        }
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statuscode = ((ApiException) e).getStatusCode();
                        switch (statuscode){

                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try{
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(SosAlert.this,REQUEST_CHECK_SETTINGS);
                                }
                                catch (IntentSender.SendIntentException sie) {
                                }
                                break;

                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage =  "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Toast.makeText(SosAlert.this,errorMessage,Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            TVlatitude.setText(String.valueOf(mCurrentLocation.getLatitude()));
            TVlongitude.setText(""+mCurrentLocation.getLongitude());
            mylatitude = TVlatitude.getText().toString();
            mylongitude = TVlongitude.getText().toString();
            insertLocation(mylatitude,mylongitude);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        // Resuming location updates depending on
        // allowed permissions
        if (mRequestingLocationUpdates & checkPermissions() & executeOnResume) {
            startLocationUpdates();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        executeOnResume = false;
        handler.removeCallbacksAndMessages(null);
        startActivity(new Intent(this,Home.class));
        finish();
    }


    private void insertLocation(final String latitude,final String longitude) {
        String tag_string_req = "insert_location";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.url_get_location
                        +sessionManager.getId(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error1");
                    if (!error) {
                        Toast.makeText(getApplicationContext(), "Called for help!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(),getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error Occurred!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        "Poor Internet Connection", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("latitude", latitude);
                params.put("longitude", longitude);
                params.put("user_id", sessionManager.getId());

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    // sending panic alert via email
    private void sendEmail() {
        String tag_string_req = "Send Email";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.url_send_email, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        Toast.makeText(getApplicationContext(), getString(R.string.email_success_message), Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.poor_internet), Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id",sessionManager.getId());
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


}