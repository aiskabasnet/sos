package com.bkc.sosapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;


    public class CallPhoneNumber {
        public static final int CALL_REQUEST = 1;

        public void callPhoneNumber(Activity activity, String phoneNumber) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling

                        ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST);

                        return;
                    }
                }

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneNumber));
                activity.startActivity(callIntent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }


