package com.bkc.sosapp.database;

public class Contacts {
    public static final String TABLE_NAME = "database_contact";

    //fields
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_PHONE = "contact";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_EMAIL = "email";


    private String id;
    private String contact,name,email;

    public static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME +
            "("+COLUMN_ID + " INTEGER PRIMARY KEY,"+
            COLUMN_PHONE + " TEXT," +
            COLUMN_NAME + " TEXT," +
            COLUMN_EMAIL + " TEXT" +
            ")";

    public Contacts(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
