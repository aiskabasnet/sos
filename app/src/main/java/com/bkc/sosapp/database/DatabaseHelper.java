package com.bkc.sosapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String  DATABASE_NAME = "wsa_db";

    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Contacts.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(" Drop Table If Exists " + Contacts.TABLE_NAME);
        onCreate(db);
    }

    //to insert contacts to sqlite
    public void insertContacts(String  id,String contact, String name, String email){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Contacts.COLUMN_ID, id);
        values.put(Contacts.COLUMN_PHONE, contact);
        values.put(Contacts.COLUMN_NAME, name);
        values.put(Contacts.COLUMN_EMAIL, email);

        db.insert(Contacts.TABLE_NAME, null, values);

        db.close();
    }

    //to fetch contacts from sqlite
    public List<Contacts> getAllContacts(){
        List<Contacts> contacts = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + Contacts.TABLE_NAME + " ORDER BY " +
                Contacts.COLUMN_ID + " ASC";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Contacts contact = new Contacts();
                contact.setId(cursor.getString(cursor.getColumnIndex(Contacts.COLUMN_ID)));
                contact.setContact(cursor.getString(cursor.getColumnIndex(Contacts.COLUMN_PHONE)));
                contact.setName(cursor.getString(cursor.getColumnIndex(Contacts.COLUMN_NAME)));
                contact.setEmail(cursor.getString(cursor.getColumnIndex(Contacts.COLUMN_EMAIL)));
                contacts.add(contact);
            } while (cursor.moveToNext());
        }
        db.close();
        cursor.close();
        return contacts;

    }
    //get count of contacts
    public int getContactsCount(){
        String countQuery = " SELECT * FROM " + Contacts.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery,null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void deleteAllContacts() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+Contacts.TABLE_NAME);
        db.close();
    }


}
