package com.bkc.sosapp;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "WSAApp";

    private static final String LATITUDE = "latitude";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String IS_FIRST_TIME_LOGGED_IN = "IsFirstLoggedIn";
    private static final String LONGITUDE = "longitude";

    public PrefManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
    public void setIsFirstTimeLoggedIn(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LOGGED_IN, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLoggedIn() {
        return pref.getBoolean(IS_FIRST_TIME_LOGGED_IN, true);
    }
    public void setLatitude(String latitude){
        editor.putString(LATITUDE, latitude);
    }

    public String  getLatitude(){
        return pref.getString(LATITUDE, null);
    }

    public void setLongitude(String latitude){
        editor.putString(LONGITUDE, latitude);
    }

    public String getLongitude(){
        return pref.getString(LONGITUDE, null);
    }
}
