package com.bkc.sosapp;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.bkc.womensecurityapp.R;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.IOException;

public class IndexActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {

    RelativeLayout manage_contact,safe_text,my_location,helpBtn, contactPolice, whistleLayout, sirenLayout, callAmbulanceLayout, callFireLayout;
    PrefManager pref;
    SessionManager session;
    boolean doubleBackToExitPressedOnce = false;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mp = new MediaPlayer();
        setTitle(getString(R.string.app_name));
        pref = new PrefManager(IndexActivity.this);
        session = new SessionManager(IndexActivity.this);

        if (pref.isFirstTimeLoggedIn()){
            Intent intent = new Intent(this, ManageContact.class);
            startActivity(intent);
        }

        safe_text = findViewById(R.id.fine);
       // my_location = findViewById(R.id.mylocation);
        helpBtn = findViewById(R.id.help);
        manage_contact = findViewById(R.id.contactRL);
        contactPolice = findViewById(R.id.contactPolice);
        callAmbulanceLayout = findViewById(R.id.callAmbulance);
        callFireLayout = findViewById(R.id.callFire);
        sirenLayout = findViewById(R.id.siren);
        whistleLayout = findViewById(R.id.whistle);
        manage_contact.setOnClickListener(this);
        safe_text.setOnClickListener(this);
//        my_location.setOnClickListener(this);
        helpBtn.setOnClickListener(this);
        contactPolice.setOnClickListener(this);
        callFireLayout.setOnClickListener(this);
        callAmbulanceLayout.setOnClickListener(this);
        whistleLayout.setOnClickListener(this);
        sirenLayout.setOnClickListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onClick(View h){
       if(h == manage_contact){
            Intent intent = new Intent(getApplicationContext(),ManageContact.class);
            startActivity(intent);
        }
        if(h == safe_text){
            Intent intent = new Intent(getApplicationContext(),SafeText.class);
            startActivity(intent);
        }
       /* if(h == my_location){
            Intent intent = new Intent(getApplicationContext(),Locations.class);
            startActivity(intent);
        }*/
        if(h == helpBtn){
            Intent intent = new Intent(getApplicationContext(),SosAlert.class);
            startActivity(intent);
        }
        if(h == contactPolice){
            CallPhoneNumber cp = new CallPhoneNumber();
            cp.callPhoneNumber(IndexActivity.this,"100");
        }
        if(h == callAmbulanceLayout){
            CallPhoneNumber cp = new CallPhoneNumber();
            cp.callPhoneNumber(IndexActivity.this,"102");
        }
        if(h == callFireLayout){
            CallPhoneNumber cp = new CallPhoneNumber();
            cp.callPhoneNumber(IndexActivity.this,"101");
        }
        if(h == whistleLayout){
            Intent intent = new Intent(getApplicationContext(),WhistleActivity.class);
            startActivity(intent);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu item) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_settings, item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout:
                session.setLogin(false);
                finishAffinity();
                Intent intent1 = new Intent(this, LoginActivity.class);
                startActivity(intent1);
                return true;

            case R.id.changePassword:
                Intent intent2 = new Intent(this, ChangePassword.class);
                startActivity(intent2);
                return true;
            case R.id.viewHistory:
                Intent i = new Intent(getApplicationContext(), MapHistoryActivity.class);
                startActivity(i);
                return true;
            case R.id.languageChange:
                Intent i1 = new Intent(getApplicationContext(), ChangeLanguage.class);
                startActivity(i1);
                return true;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
          if (doubleBackToExitPressedOnce) {
            finishAffinity();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.changePassword) {
            // Handle the camera action
        } else if (id == R.id.languageChange) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
