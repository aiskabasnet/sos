package com.bkc.sosapp;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.bkc.womensecurityapp.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.bkc.sosapp.database.Contacts;
import com.bkc.sosapp.database.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ManageContact extends AppCompatActivity{

    private final int CALL_REQUEST = 100;
    CallPhoneNumber cp = new CallPhoneNumber();
    FloatingActionButton addContacts;
    private DatabaseHelper db;
    private List<Contacts> contactsDetails;
    String[] id, name, email, contact;
    int count;
    public LocationManager locationManager;
    SessionManager sessionManager;
    String title;
    SimpleAdapter adapter;
    ListView listview;
    private SearchView searchView;
    private List<HashMap<String, String>> aList;
    SessionManager session;
    private ShimmerFrameLayout mShimmerViewContainer;
    private ProgressDialog pDialog;
    TextView noContactTV;
    PrefManager pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.manage_contacts));
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        pref = new PrefManager(ManageContact.this);
        pref.setIsFirstTimeLoggedIn(false);
        pDialog = new ProgressDialog(this);
        sessionManager = new SessionManager(ManageContact.this);

        db = new DatabaseHelper(ManageContact.this);
        contactsDetails = new ArrayList<>();
        session = new SessionManager(ManageContact.this);
        listview = (ListView) this.findViewById(R.id.listview);
        addContacts = (FloatingActionButton) this.findViewById(R.id.addContact);

        noContactTV = findViewById(R.id.noData);

        addContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddContacts.class);
                intent.putExtra("action","add");
                startActivity(intent);
            }
        });
        if (CheckNetwork.isInternetAvailable(getApplicationContext())){
            listview.setVisibility(View.VISIBLE);
            initialize();
        }
        else{
            stopShimmer();
            addContacts.setVisibility(View.INVISIBLE);

            if (db.getContactsCount() == 0) {
                listview.setVisibility(View.INVISIBLE);
            }
            else{
                fromMobileDatabase();
            }
        }

        if (pref.isFirstTimeLoggedIn()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        else{
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public static boolean isLocationEnabled(Context context) {

        return true;
    }


    public void fromMobileDatabase(){
        listview.setVisibility(View.VISIBLE);
        contactsDetails = db.getAllContacts();
        count = db.getContactsCount();
        id = new String[count];
        name = new String[count];
        email = new String[count];
        contact = new String[count];
        for(int i=0;i<count;i++){
            id[i] = contactsDetails.get(i).getId();
            name[i] = contactsDetails.get(i).getName();
            email[i] = contactsDetails.get(i).getEmail();
            contact[i] = contactsDetails.get(i).getContact();
        }
        listinitialize();
    }

    private void stopShimmer(){
        mShimmerViewContainer.stopShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.GONE);
    }
    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    private void initialize() {
        String tag_string_req = "View Contacts";
        JsonArrayRequest strReq = new JsonArrayRequest(
                AppConfig.url_view_contacts+sessionManager.getId(),new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    count = response.length();
                    id = new String[count];
                    name = new String[count];
                    email = new String[count];
                    contact = new String[count];
                    db.deleteAllContacts();
                    if (response.length()>=5){
                        addContacts.setVisibility(View.GONE);
                    }
                    else {
                        addContacts.setVisibility(View.VISIBLE);
                    }

                    for (int i=0;i<response.length();i++){
                        JSONObject jObj = response.getJSONObject(i);
                        boolean error = jObj.getBoolean("error");
                        if (!error) {
                            id[i] = jObj.getString("id");
                            name[i] = jObj.getString("name");
                            email[i] = jObj.getString("email");
                            contact[i] = jObj.getString("contact");
                            db.insertContacts(id[i],contact[i],name[i],email[i]);

                            Log.e("contactCheck",""+id[i]);

                        }
                        else{
                            noContactTV.setVisibility(View.VISIBLE);
                            listview.setVisibility(View.INVISIBLE);
                        }
                    }
                    if (id[0] != null) {
                        listinitialize();
                    }
                    stopShimmer();
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopShimmer();
                    Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                stopShimmer();
                fromMobileDatabase();
                Toast.makeText(getApplicationContext(),
                        "Poor Network Connection", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    private void listinitialize() {
        aList = new ArrayList<>();
        for (int i=0; i<count;i++){
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("id", id[i]);
            if (name[i].length()==0){
                hm.put("index", " ");
            }
            else {
                hm.put("index", (name[i].substring(0, 1)).toUpperCase());
            }
            // Log.e("ofjhoisg ",name[i].length()+"");
            hm.put("name", name[i]);
            hm.put("email", email[i]);
            hm.put("contact", contact[i]);
            aList.add(hm);
        }
        String[] from = {
                "index","name","email","contact"
        };
        int[] to = {
                R.id.firstnameText,
                R.id.name,
                R.id.contact,
                R.id.email,
        };
        adapter = new SimpleAdapter(getBaseContext(),aList, R.layout.list_contacts_details,from,to);
        listview.setAdapter(adapter);
        stopShimmer();


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                registerForContextMenu(listview);
                view.showContextMenu();
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        if(CheckNetwork.isInternetAvailable(ManageContact.this)) {
            inflater.inflate(R.menu.menu_contacts, menu);
        }
        else {
            inflater.inflate(R.menu.menu_contacts_offline, menu);
        }
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(name[info.position]);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.call:
                Log.e(getPackageName(),"Phone :"+contact[info.position]);
                cp.callPhoneNumber(ManageContact.this, contact[info.position]);
                return true;
            case R.id.edit:
                Intent intent = new Intent(getApplicationContext(), AddContacts.class);
                intent.putExtra("action","edit");
                intent.putExtra("id",id[info.position]);
                intent.putExtra("name",name[info.position]);
                intent.putExtra("email",email[info.position]);
                intent.putExtra("contact", contact[info.position]);
                startActivity(intent);
                return true;

            case R.id.delete:
                if (CheckNetwork.isInternetAvailable(getApplicationContext())) {
                    deleteContacts(id[info.position]);
                }else{
                    Toast.makeText(getApplicationContext(),getString(R.string.no_internet),Toast.LENGTH_SHORT).show();
                }


            default:
                return false;

        }

    }

    private void deleteContacts(final String id) {
        String tag_string_req = "Delete Contacts";
        pDialog.setMessage("Deleting ...");
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.url_delete_contacts+id, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        String message = getString(R.string.success_delete);
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        initialize();
                    } else {
                        String errorMsg = getString(R.string.error_delete);
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Error Occured!", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                Toast.makeText(getApplicationContext(),
                        getString(R.string.poor_internet), Toast.LENGTH_SHORT).show();
            }
        }) {

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                ManageContact.this.adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                ManageContact.this.adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        if(requestCode == CALL_REQUEST)
        {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                cp.callPhoneNumber(ManageContact.this,"000000");
            }
            else
            {
                Toast.makeText(ManageContact.this, getResources().getString(R.string.call_permission_denied_message), Toast.LENGTH_SHORT).show();
            }
        }
    }
}