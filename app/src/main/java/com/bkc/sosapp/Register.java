package com.bkc.sosapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bkc.womensecurityapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity implements View.OnClickListener {

    EditText fname, lname, uname, regemail, mobile, pwd, cpwd;
    Button registerbtn;
    String first, last, user, mail, mob, chkpwd, conpwd;
    TextView redirect;
    private ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fname = (EditText) findViewById(R.id.first_name);
        lname = (EditText) findViewById(R.id.last_name);
        uname = (EditText) findViewById(R.id.user_name);
        regemail = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobileno);
        pwd = (EditText) findViewById(R.id.reg_password);
        cpwd = (EditText) findViewById(R.id.confirm_pwd);
        redirect = findViewById(R.id.redirectlogin);

        pDialog = new ProgressDialog(this);


        registerbtn = (Button) findViewById(R.id.register);
        registerbtn.setOnClickListener(this);
        redirect.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onClick(View v) {
        if(v == redirect){
            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(intent);
        }

        if (v == registerbtn) {
            if(CheckNetwork.isInternetAvailable(Register.this)) {
                boolean valid;
                valid = validate();
                if (valid) {
                    registerUser(first,last,user,mob,mail,chkpwd);
                }
            }else {
                Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean validate() {
        boolean valid = true;
        first = fname.getText().toString();
        last = lname.getText().toString();
        user = uname.getText().toString();
        mail = regemail.getText().toString();
        mob = mobile.getText().toString();
        chkpwd = pwd.getText().toString();
        conpwd = cpwd.getText().toString();

        if (first.isEmpty()) {
            fname.setError("First name cannot be left blank!!!");
            valid = false;
        }
        if (last.isEmpty()) {
            lname.setError("Last name cannot be left blank!!!");
            valid = false;
        }
        if (user.isEmpty()){
            uname.setError("User-name cannot be left blank!!!");
            valid = false;
        }
        if (mob.isEmpty()){
            mobile.setError("Mobile number cannot be left blank!!!");
            valid = false;
        } else if(mob != null){
            if (isValidPhoneNumber(mob)){
            } else{
                valid = false;
                mobile.setError("Phone number must be of 10 digits");
            }
        }
        if(mail.isEmpty()){
            valid = true;
        }
        else if(mail != null){
            if (isValidMail(mail)) {
                valid = true;
            } else {
                valid = false;
                regemail.setError("Invalid email address");
            }

        }


        if (conpwd.contentEquals(chkpwd)) {
            if (chkpwd.isEmpty()) {
                pwd.setError("Password cannot be left blank!!!");
                valid = false;
            }
        } else {
            Toast.makeText(getApplication(), "Password didn't matched", Toast.LENGTH_LONG).show();
            pwd.setError("Password didn't matched");
            cpwd.setError("Password didn't matched");
            valid = false;
        }

        return valid;
    }

    private boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static boolean isValidPhoneNumber(String phoneNumber) {
        if(phoneNumber.length()<=9 || phoneNumber.length()>=11){
            return false;
        }
        return true;
    }


    private void registerUser(final String fname, final String lname, final String username,
                              final String mobile, final String email, final String rpassword) {
        // Tag used to cancel the request
        String tag_string_req = "register";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.url_registration, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error1");
                    if (!error) {
                        // User successfully stored in MySQL


                        Toast.makeText(getApplicationContext(), "User successfully registered. Try login now!", Toast.LENGTH_LONG).show();

                        // Launch login activity
                        Intent intent = new Intent(
                                Register.this,
                                LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        Toast.makeText(getApplicationContext(),
                                "Error", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Error Occured!!!", Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("first_name", fname);
                params.put("last_name", lname);
                params.put("username", username);
                params.put("contact",mobile);
                params.put("email",email);
                params.put("password",rpassword);
                params.put("submit","1");

               return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}

