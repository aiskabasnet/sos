package com.bkc.sosapp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bkc.womensecurityapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import com.google.android.gms.maps.model.MarkerOptions;
public class MapHistoryActivity extends AppCompatActivity implements com.google.android.gms.maps.OnMapReadyCallback, View.OnClickListener {

    Calendar myCalendar1, myCalendar2;
    Button fromButton, toButton;
     private com.google.android.gms.maps.GoogleMap mMap;
    ImageButton searchButton;
    DatePickerDialog.OnDateSetListener date1, date2;
    LinearLayout mapLayout;
    String from,to;
    private ProgressDialog pDialog;
    int count;
    com.google.android.gms.maps.SupportMapFragment mapFragment;
    String[] latitude, longitude;
    SessionManager sessionManager;
    MarkerOptions[] markers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mapFragment = (com.google.android.gms.maps.SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        sessionManager = new SessionManager(MapHistoryActivity.this);
        pDialog = new ProgressDialog(this);
        mapLayout = findViewById(R.id.l2);
        mapLayout.setVisibility(View.GONE);
        fromButton = findViewById(R.id.from);
        toButton = findViewById(R.id.to);
        searchButton = findViewById(R.id.search);
        myCalendar1 = Calendar.getInstance();
        myCalendar2 = Calendar.getInstance();
        date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar1.set(Calendar.YEAR, year);
                myCalendar1.set(Calendar.MONTH, monthOfYear);
                myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel1();

            }

        };
        date2 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar2.set(Calendar.YEAR, year);
                myCalendar2.set(Calendar.MONTH, monthOfYear);
                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel2();

            }

        };
        fromButton.setOnClickListener(this);
        toButton.setOnClickListener(this);
        searchButton.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void updateLabel1() {
        String myFormat = "yy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        from = sdf.format(myCalendar1.getTime());
        fromButton.setText(sdf.format(myCalendar1.getTime()));
    }

    private void updateLabel2() {

        String myFormat = "yy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        to = sdf.format(myCalendar2.getTime());
        toButton.setText(sdf.format(myCalendar2.getTime()));
    }
@Override
    public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
        mMap = googleMap;

    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        if(view==fromButton){
            new DatePickerDialog(MapHistoryActivity.this, date1, myCalendar1
                    .get(Calendar.YEAR), myCalendar1.get(Calendar.MONTH),
                    myCalendar1.get(Calendar.DAY_OF_MONTH)).show();

        }
        else if(view == toButton){
            new DatePickerDialog(MapHistoryActivity.this, date2, myCalendar2
                    .get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH),
                    myCalendar2.get(Calendar.DAY_OF_MONTH)).show();

        }else if(view == searchButton){
            boolean v = validate();
            if(v) {
                search();
            }
        }
    }

    private boolean validate() {
        boolean v = true;
        if (fromButton.getText().equals("FROM")){
            Toast.makeText(this, "Please choose a valid FROM date",Toast.LENGTH_SHORT).show();
            v = false;
        }

        if (toButton.getText().equals("TO")){
            Toast.makeText(this, "Please choose a valid TO date",Toast.LENGTH_SHORT).show();
            v = false;
        }
        return v;
    }

    private void search() {
        String tag_string_req = "View Contacts";
        pDialog.setMessage("Please wait...");
        //showDialog();
        from = "20"+from;
        to = "20"+to;
        count = 5;
        latitude = new String[count];
        longitude = new String[count];
        latitude[0]="26.4625452";
        latitude[1]="26.4725452";
        latitude[2]="26.4825452";
        latitude[3]="26.4925452";
        latitude[4]="26.5025452";

        longitude[0]="87.2942213";
        longitude[1]="87.3042213";
        longitude[2]="87.3142213";
        longitude[3]="87.3242213";
        longitude[4]="87.3342213";
        mapInitialize();

        /*JsonArrayRequest strReq = new JsonArrayRequest(
                AppConfig.url_get_history+sessionManager.getId()+"&from="+from+"&to="+to,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e("response",response.toString());
                try {
                    count = response.length();
                    latitude = new String[count];
                    longitude = new String[count];
                    for (int i=0;i<response.length();i++){
                        JSONObject jObj = response.getJSONObject(i);
                        boolean error = jObj.getBoolean("error");
                        if (!error) {
                            latitude[i] = jObj.getString("latitude");
                            longitude[i] = jObj.getString("longitude");
                        }
                        else{
                            mapLayout.setVisibility(View.GONE);
                        }
                    }
                    if (latitude[0] != null) {
                        mapLayout.setVisibility(View.VISIBLE);
                        mapInitialize();
                    }
                    hideDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideDialog();
                    Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                hideDialog();
                Toast.makeText(getApplicationContext(),
                        getString(R.string.poor_internet), Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);*/
    }

    private void mapInitialize() {
        mapLayout.setVisibility(View.VISIBLE);
        markers = new MarkerOptions[count];
        markers[0] = new com.google.android.gms.maps.model.MarkerOptions().position(new com.google.android.gms.maps.model.LatLng(Double.parseDouble(latitude[0]), Double.parseDouble(longitude[0]))).title("");
        markers[1] = new com.google.android.gms.maps.model.MarkerOptions().position(new com.google.android.gms.maps.model.LatLng(Double.parseDouble(latitude[1]), Double.parseDouble(longitude[1]))).title("");
        markers[2] = new com.google.android.gms.maps.model.MarkerOptions().position(new com.google.android.gms.maps.model.LatLng(Double.parseDouble(latitude[2]), Double.parseDouble(longitude[2]))).title("");
        markers[3] = new com.google.android.gms.maps.model.MarkerOptions().position(new com.google.android.gms.maps.model.LatLng(Double.parseDouble(latitude[3]), Double.parseDouble(longitude[3]))).title("");
        markers[4] = new com.google.android.gms.maps.model.MarkerOptions().position(new com.google.android.gms.maps.model.LatLng(Double.parseDouble(latitude[4]), Double.parseDouble(longitude[4]))).title("");

        //Adding the created marker on the map
        mMap.addMarker(markers[0]);
        mMap.addMarker(markers[1]);
        mMap.addMarker(markers[2]);
        mMap.addMarker(markers[3]);
        mMap.addMarker(markers[4]);
        com.google.android.gms.maps.CameraUpdate cameraUpdate = com.google.android.gms.maps.CameraUpdateFactory.newLatLngZoom(new com.google.android.gms.maps.model.LatLng(Double.parseDouble(latitude[0]), Double.parseDouble(longitude[0])), 14f);
        mMap.animateCamera(cameraUpdate);

    }
}
