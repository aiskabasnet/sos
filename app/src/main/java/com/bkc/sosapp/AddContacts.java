package com.bkc.sosapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bkc.womensecurityapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddContacts extends AppCompatActivity implements View.OnClickListener {

    EditText contactET,nameET,emailET;
    Button contactSaveBtn;
    ImageView contactPicker,close_btn;
    private ProgressDialog pDialog;
    private final int REQUEST_CODE = 00;
    String num,eContact,eName,eEmail,action,id;
    SessionManager sessionManager;
    RelativeLayout textLayout;
    PrefManager pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contacts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pref = new PrefManager(this);
        sessionManager = new SessionManager(this);
        pDialog = new ProgressDialog(this);

        action = getIntent().getStringExtra("action");
        textLayout = findViewById(R.id.layoutDescription);
        close_btn = findViewById(R.id.cross_btn);
        contactPicker = findViewById(R.id.insert_contact_picker);
        contactET = findViewById(R.id.insert_contact);
        nameET = findViewById(R.id.insert_contact_name);
        emailET = findViewById(R.id.insert_contact_email);
        contactSaveBtn = findViewById(R.id.insert_contact_btn);

        contactPicker.setOnClickListener(this);
        contactSaveBtn.setOnClickListener(this);
        close_btn.setOnClickListener(this);

        if(action.equals("add")){
            contactSaveBtn.setText(getString(R.string.add));
            setTitle("Add Emergency Contact");
        }
        else {
            contactSaveBtn.setText(R.string.edit);
            id = getIntent().getStringExtra("id");
            eName = getIntent().getStringExtra("name");
            eEmail= getIntent().getStringExtra("email");
            eContact= getIntent().getStringExtra("contact");

            setTitle(eName);
            nameET.setText(eName);
            emailET.setText(eEmail);
            contactET.setText(eContact);
            textLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view){
        if(view == close_btn){
            textLayout.setVisibility(View.GONE);
        }

        if(view == contactPicker){
            Intent intent = new Intent(Intent.ACTION_PICK,ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent,REQUEST_CODE);
        }

        if(view == contactSaveBtn){
            if(CheckNetwork.isInternetAvailable(AddContacts.this)){
                boolean valid;
                valid = validateInsertContact();
                if(valid){
                    if(action.equals("add")) {
                        emergencyContacts(eName, eContact, eEmail);
                    }
                    else {
                        updateContacts();
                    }
                }
            }
        }
    }


    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case (REQUEST_CODE):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (Integer.valueOf(hasNumber) == 1) {
                            Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                            while (numbers.moveToNext()) {
                                num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                contactET.setText(num);
                            }
                        }
                    }
                    break;
                }
        }
    }

    public boolean validateInsertContact(){
        boolean valid = true;
        eName = nameET.getText().toString();
        eContact = contactET.getText().toString();
        eEmail = emailET.getText().toString();

        if(eName.isEmpty()){
            nameET.setError("Please enter name");
            valid = false;
        }
        if(eContact.isEmpty()){
            contactET.setError("Please enter or pick a contact number");
            valid = false;
        }
        if(eEmail.isEmpty()){
            valid = true;
        }
        else {
            if(isValidMail(eEmail)){
                valid = true;
            }
            else {
                emailET.setError("Invalid Email address");
            }
        }
        return valid;
    }

    private boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void emergencyContacts(final String name, final String contact, final String e_mail) {
        // Tag used to cancel the request
        String tag_string_req = "Emergency Contacts";

        pDialog.setMessage("Inserting...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.url_contacts, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {

                        Toast.makeText(getApplicationContext(), "Successfully Inserted!", Toast.LENGTH_LONG).show();

                        pref.setIsFirstTimeLoggedIn(true);
                        Intent intent = new Intent(
                                AddContacts.this,
                                ManageContact.class);
                        startActivity(intent);
                        finish();
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        Toast.makeText(getApplicationContext(),
                                "Error", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Error Occured!", Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",sessionManager.getId());
                params.put("contact", contact);
                params.put("name", name);
                params.put("email", e_mail);
                params.put("submit","1");

                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void goBack(){
        Intent intent = new Intent(getApplicationContext(),ManageContact.class);
        startActivity(intent);
    }

    private void updateContacts() {
        String tag_string_req = "Edit Contacts";
        pDialog.setMessage("Please wait ...");
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.url_update_contacts+id, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    Log.e("Edit",""+response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        String message = "Successfully Edited!";
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        finish();
                        goBack();

                    } else {
                        String errorMsg = "Error Occured!";
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(getApplicationContext(),
                        getString(R.string.poor_internet), Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",sessionManager.getId());
                params.put("name", eName);
                params.put("contact", eContact);
                params.put("email", eEmail);
                params.put("submit","1");

                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
