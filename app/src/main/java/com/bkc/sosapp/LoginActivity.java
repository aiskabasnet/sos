package com.bkc.sosapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bkc.womensecurityapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    EditText uname,pass;
    Button loginbtn,signupbtn;
    String user_name_check,password;
    private ProgressDialog pDialog;
    SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(this);
        if (session.isLoggedIn()){
            Intent intent = new Intent(getApplicationContext(),IndexActivity.class);
            startActivity(intent);
        }
        else {

            setContentView(R.layout.activity_main);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            uname = (EditText) findViewById(R.id.user_name);
            pass = (EditText) findViewById(R.id.password);
            loginbtn = (Button) findViewById(R.id.login);
            signupbtn = (Button) findViewById(R.id.signup);
            loginbtn.setOnClickListener(this);
            signupbtn.setOnClickListener(this);
            pDialog = new ProgressDialog(this);
            pDialog.setCancelable(false);
        }
    }



    @Override
            public void onClick(View v1) {
                if (v1 == loginbtn) {
                    if(CheckNetwork.isInternetAvailable(LoginActivity.this)) {
                        boolean valid;
                        valid = validation();
                        if (valid) {
                            checklogin(user_name_check,password);
                        }
                    }else {
                        Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                }
                }

                if(v1 == signupbtn){
                    Intent intent = new Intent(getApplicationContext(), Register.class);
                    startActivity(intent);
                }
            }

            private boolean validation() {
                boolean valid = true;
                user_name_check = uname.getText().toString();
                password = pass.getText().toString();

                if(user_name_check.isEmpty()){
                    uname.setError("Username cannot be left blank!!!");
                    valid = false;
                }
                if(password.isEmpty()){
                    pass.setError("Password cannot be left blank!!!");
                    valid = false;
                }
               return valid;
            }

    private void checklogin(final String user_name_check, final String password) {
        String tag_string_req = "login";
        pDialog.setMessage("Logging in ...");
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.url_login, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        String  id = jObj.getString("user_id");

                        String message = "Login Successful";
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        finish();
                        Intent intent = new Intent(LoginActivity.this, IndexActivity.class);
                        startActivity(intent);
                        session.setLogin(true);
                        session.setId(id);
                        Log.e("id ",""+session.getId());

                    } else {
                        Toast.makeText(getApplicationContext(),"Invalid Username or Password", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", user_name_check);
                params.put("password", password);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
