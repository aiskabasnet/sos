package com.bkc.sosapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bkc.womensecurityapp.R;

public class Home extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout manage_contact,safe_text,my_location,helpBtn, contactPolice;
    PrefManager pref;
    SessionManager session;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pref = new PrefManager(Home.this);
        session = new SessionManager(Home.this);

        if (pref.isFirstTimeLoggedIn()){
            Intent intent = new Intent(this, ManageContact.class);
            startActivity(intent);
        }

        safe_text = findViewById(R.id.fine);
       // my_location = findViewById(R.id.mylocation);
        helpBtn = findViewById(R.id.help);
        manage_contact = findViewById(R.id.contactRL);
        contactPolice = findViewById(R.id.contactPolice);

        manage_contact.setOnClickListener(this);
        safe_text.setOnClickListener(this);
        //my_location.setOnClickListener(this);
        helpBtn.setOnClickListener(this);
        contactPolice.setOnClickListener(this);
    }

    @Override
    public void onClick(View h){
       if(h == manage_contact){
            Intent intent = new Intent(getApplicationContext(),ManageContact.class);
            startActivity(intent);
        }
        if(h == safe_text){
            Intent intent = new Intent(getApplicationContext(),SafeText.class);
            startActivity(intent);
        }
        if(h == my_location){
            Intent intent = new Intent(getApplicationContext(),Locations.class);
            startActivity(intent);
        }
        if(h == helpBtn){
            Intent intent = new Intent(getApplicationContext(),SosAlert.class);
            startActivity(intent);
        }
        if(h == contactPolice){
            CallPhoneNumber cp = new CallPhoneNumber();
            cp.callPhoneNumber(Home.this,"100");
        }
    }

    @Override
    public void onBackPressed(){
        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu item) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_settings, item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout:
                session.setLogin(false);;
                finishAffinity();
                Intent intent1 = new Intent(this, LoginActivity.class);
                startActivity(intent1);
                return true;
            case R.id.viewHistory:
                Intent i = new Intent(getApplicationContext(), MapHistoryActivity.class);
                startActivity(i);
                return true;
            case R.id.languageChange:
                Intent i1 = new Intent(getApplicationContext(), ChangeLanguage.class);
                startActivity(i1);
                return true;
        }
        return true;
    }
}
