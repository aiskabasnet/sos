package com.bkc.sosapp;

import java.security.PublicKey;

public class AppConfig {

    public static String url_login = "http://pdmt.com.np/wsa/login.php";
    public static String url_registration = "http://pdmt.com.np/wsa/registration.php";
    public static String url_contacts = "http://pdmt.com.np/wsa/emergency_contacts.php?is_mobile=1";
    public static String url_delete_contacts = "http://pdmt.com.np/wsa/emergency_contacts_delete.php?is_mobile=1&id=";
    public static String url_update_contacts = "http://pdmt.com.np/wsa/emergency_contacts_edit.php?is_mobile=1&id=";
    public static String url_view_contacts = "http://pdmt.com.np/wsa/view_emergency_contacts.php?is_mobile=1&id=";
    public static String url_location = "http://pdmt.com.np/wsa/show_location";
    public static String url_get_location = "http://pdmt.com.np/wsa/get_location.php?id=";
    public static String url_send_email = "http://pdmt.com.np/wsa/test.php";
    public static String url_update_location = "http://pdmt.com.np/wsa/update_location.php?id=";


    public static String url_get_history="http://pdmt.com.np/wsa/get_location_history.php?id=";
}
