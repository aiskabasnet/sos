package com.bkc.sosapp;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bkc.womensecurityapp.R;

import java.io.IOException;

public class WhistleActivity extends AppCompatActivity {

    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whistle);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        imageView = findViewById(R.id.whistle);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayer mPlayer = MediaPlayer.create(WhistleActivity.this, R.raw.whistle);

                Log.e("dlkh","fn");
                try {

                    mPlayer.prepare();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if(mPlayer.isLooping()){
                    Log.e("dlkh","stop");

                    mPlayer.stop();
                    imageView.setImageResource(R.drawable.whistle1);
                }else{
                    Log.e("dlkh","start");

                    mPlayer.start();
                    imageView.setImageResource(R.drawable.whistle);
                }

            }
        });
    }

}
